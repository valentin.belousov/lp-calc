export type ContractReserves = {
  _reserve0: string;
  _reserve1: string;
};

export type ContractData = {
  globTokensPrice: string;
  globTokenPrice: string;
  totalSupply: string;
};
