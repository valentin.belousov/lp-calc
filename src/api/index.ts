import { CONTRACT_ADDRESS, FTM_SCAN_API_KEY } from "@/utils";
import { AbiItem } from "web3-utils";

const baseFetch = (url: string) =>
  fetch(url).then((response) => {
    if (!response.ok) {
      throw new Error("An error has occurred.");
    }
    return response.json();
  });

export const getContractABI = (): Promise<AbiItem> =>
  baseFetch(
    `https://api.ftmscan.com/api?module=contract&action=getabi&address=${CONTRACT_ADDRESS}&apikey=${FTM_SCAN_API_KEY}`
  ).then(({ result }) => JSON.parse(result));

export const getTokenPrice = (
  tokenAddress: string,
  vsCurrency = "usd"
): Promise<number> =>
  baseFetch(
    `https://api.coingecko.com/api/v3/simple/token_price/fantom?contract_addresses=${tokenAddress}&vs_currencies=${vsCurrency}`
  ).then((res) => res[tokenAddress.toLowerCase()][vsCurrency]);
